# My Component

This repo contains some gitlab component for experimental purpose

## Cloud Deploy (Experimental)
The `cloud-deploy` components create a Cloud Deploy `release`. There are 2 flavors of the component: `cloud-deploy-img` and `cloud-deploy-script`.

### Cloud Deploy Image ([cloud-deploy src][cd-src], [google-cloud-auth src][gca-src]).
[cloud-deploy-img](./templates/cloud-deploy-img.yml) is a customized go binary based solution which is built on [Cloud Client Library](https://cloud.google.com/apis/docs/cloud-client-libraries).

The component:
  - Use `google-cloud-auth` command to generate credentials file to authenticate the cloud clients
  - Use `cloud-deploy release` command (based on [cloud client libraries]((https://cloud.google.com/go/docs/reference/cloud.google.com/go/deploy/latest/apiv1))) to create a release.

### Inputs
#### Authentication Inputs
##### Workload Identity Federation
The following inputs are for _authenticating_ to Google Cloud via Workload
Identity Federation.

-   `workload-identity-provider`: (Required) The full identifier of the Workload
    Identity Provider, including the project number, pool name, and provider
    name. If provided, this must be the full identifier which includes all
    parts:

    ```text
    projects/123456789/locations/global/workloadIdentityPools/my-pool/providers/my-provider
    ```

    **Note**: `workload-identity-provider` cannot coexist with `credentials-file`.


-   `gcp-oidc-jwt`: (Required) The full OIDC JWT provided by Gitlab, can be found as `id_tokens.GCP_OIDC_JWT` in the
     Gitlab CI/CD config.

    ```text
    id_tokens:
        GCP_OIDC_JWT:
        aud: ...
    ```

-   `service-account`: (Optional) Email address or unique identifier of the
    Google Cloud service account for which to impersonate and generate
    credentials. For example:

    ```text
    my-service-account@my-project.iam.gserviceaccount.com
    ```

    Without this input, the Gitlab Components using this binary will use Direct Workload Identity
    Federation. If this input is provided, the Gitlab Components will use
    Workload Identity Federation through a Service Account.

##### Inputs: Credential Files
The following inputs are for _authenticating_ to Google Cloud via user provided crendentials file.
-   `credentials-file`: (Required) The name of the credential file that user uploaded via [Gitlab Secure File][secure-file].

    **Note**: `credentials-file` cannot coexist with `workload-identity-provider`.

#### Cloud Deploy Inputs
-   `gcp-project`: (Required) ...
-   `release`: (Required)...
-   `delivery_pipeline`: (Required)...
-   `source`: (Required)...
-   `region`: (Required)...

Usage can be found in this [example][example].

### Cloud Deploy Script.
[cloud-deploy-script](./templates/cloud-deploy-script.yml) leverages gcloud sdk to create a Cloud Deploy release directly:
  - Use shell scripts to setup Workload Identity Federation
  - run `gcloud deploy release create ...`

**Note**: This is a prototype which supports very limited usecase and functionalities for demo purpose only. Please **DO NOT** use this component.

[cd-src]: https://gitlab.com/quanzhang/cloud-deploy
[gca-src]: https://gitlab.com/quanzhang/google-cloud-auth
[example]: https://gitlab.com/quanzhang/hello/-/blob/cloud-deploy/.gitlab-ci.yml?ref_type=948b0a36b59cb792192706e22d6d2f6fa316aae8